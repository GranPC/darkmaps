package com.peniscorp.darkmaps;

import android.util.Log;

import static de.robv.android.xposed.XposedHelpers.findAndHookConstructor;
import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class MapsHook implements IXposedHookLoadPackage
{
    public void handleLoadPackage( final LoadPackageParam lpparam ) throws Throwable
    {
        if ( !lpparam.packageName.equals( "com.google.android.apps.maps" ) )
            return;

        findAndHookConstructor( "com.google.android.apps.gmm.map.events.ar", lpparam.classLoader, String.class, int.class, boolean.class, new XC_MethodHook()
        {
            @Override
            protected void beforeHookedMethod( MethodHookParam param ) throws Throwable
            {
                Log.d( "MapsDark", "constructor called with " + param.args[ 0 ] + " mode" );
                param.args[ 2 ] = true; // NIGHT!
            }
            @Override
            protected void afterHookedMethod( MethodHookParam param ) throws Throwable
            {
            }
        } );

        findAndHookMethod( "com.google.android.apps.gmm.map.events.ar", lpparam.classLoader, "a", boolean.class, new XC_MethodHook()
        {
            @Override
            protected void beforeHookedMethod( MethodHookParam param ) throws Throwable
            {
                Log.d( "MapsDark", "this other thing got called" );

                param.args[ 0 ] = true; // NIGHT!
            }
            @Override
            protected void afterHookedMethod( MethodHookParam param ) throws Throwable
            {
            }
        } );
    }
}